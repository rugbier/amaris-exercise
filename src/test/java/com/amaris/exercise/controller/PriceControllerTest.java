package com.amaris.exercise.controller;

import com.amaris.exercise.dto.PriceDto;
import io.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.ParseException;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PriceControllerTest {
    private static final String BRAND_ID_PARAM_NAME = "brandId";
    private static final String PRODUCT_ID_PARAM_NAME = "productId";
    private static final String DATE_PARAM_NAME = "date";
    private static final String SERVER_URL = "http://localhost:%d/";
    private static final String PRICES_ENDPOINT = String.format("/price/{%s}/{%s}", BRAND_ID_PARAM_NAME, PRODUCT_ID_PARAM_NAME);

    @LocalServerPort
    private int port;

    @BeforeEach
    private void beaforeEach() {
        RestAssured.baseURI = String.format(SERVER_URL, port);
    }

    @ParameterizedTest
    @MethodSource("parametrizedTestArguments")
    void test_checkPriceEndPointByBrandAndProductAndDate(Integer brandId, Integer productId, String date, Double expectedPrice) {
        PriceDto responseDto = RestAssured.given().
                pathParam(BRAND_ID_PARAM_NAME, brandId).
                pathParam(PRODUCT_ID_PARAM_NAME, productId).
                queryParam(DATE_PARAM_NAME, date).
                log().all().
                get(PRICES_ENDPOINT).
                then().log().all().
                and().assertThat().statusCode(HttpStatus.SC_OK).
                extract().as(PriceDto.class);

        assertThat(responseDto.getPrice()).isEqualTo(expectedPrice);

    }

    private static Stream<Arguments> parametrizedTestArguments() throws ParseException {
        return Stream.of(
                Arguments.of(1, 35455, "2020-06-14-10.00.00", 35.5),
                Arguments.of(1, 35455, "2020-06-14-16.00.00", 25.45),
                Arguments.of(1, 35455, "2020-06-14-21.00.00", 35.5),
                Arguments.of(1, 35455, "2020-06-15-10.00.00", 30.5),
                Arguments.of(1, 35455, "2020-06-16-21.00.00", 38.95)
        );
    }
}
