package com.amaris.exercise.controller;

import com.amaris.exercise.dto.PriceDto;
import com.amaris.exercise.service.PriceService;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
public class PriceController {

    private final PriceService priceService;

    @GetMapping("/price/{brandId}/{productId}")
    PriceDto getPriceForBrandAndProduct(@PathVariable Integer brandId,
                                        @PathVariable Integer productId,
                                        @RequestParam(value = "date") @DateTimeFormat(pattern = "yyyy-MM-dd-HH.mm.ss") Date date){
        return priceService.getPriceForProductAndBrand(brandId, productId, date);
    }

    @GetMapping("/price")
    List<PriceDto> getAllPrices(){
        return priceService.getAllPrices();
    }

}
