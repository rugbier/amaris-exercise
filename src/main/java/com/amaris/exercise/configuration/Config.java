package com.amaris.exercise.configuration;

import com.amaris.exercise.model.Price;
import com.amaris.exercise.repository.PriceRepository;
import com.amaris.exercise.utils.CsvReaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.util.List;

@Configuration
@EnableScheduling
public class Config {

    private static final Logger log = LoggerFactory.getLogger(Config.class);

    @Autowired
    private PriceRepository repository;

    @Bean
    CommandLineRunner initDatabase() {

        return args -> {
            log.info("Loading DataBase");
            CsvReaderUtil.csvToPrice(new ClassPathResource("prices.csv").getFile().getPath()).
                    forEach(p -> repository.saveAndFlush(p));
        };
    }

    @Scheduled(cron = "0 0 0 * * ?", zone = "Europe/Madrid")//00:00 Hs.
    public void scheduledDataBaseReload() {
        try {
            List<Price> priceList = CsvReaderUtil.csvToPrice(new ClassPathResource("prices.csv").getFile().getPath());
            log.info("Cleaning data base");
            repository.deleteAll();
            log.info("Loading data");
            priceList.forEach(p -> repository.saveAndFlush(p));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
