package com.amaris.exercise.service;

import com.amaris.exercise.dto.PriceDto;
import com.amaris.exercise.mappers.PriceMapper;
import com.amaris.exercise.repository.PriceRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PriceService {
    private final PriceRepository priceRepository;

    public List<PriceDto> getAllPrices(){
        return priceRepository.findAll().stream().map(PriceMapper.INSTANCE::toPriceDto).collect(Collectors.toList());
    }

    public PriceDto getPriceForProductAndBrand(Integer brandId, Integer productId, Date date){
        return PriceMapper.INSTANCE.toPriceDto(priceRepository.getPriceForProductAndBrand(brandId, productId, date));
    }

}
