package com.amaris.exercise.repository;


import com.amaris.exercise.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

public interface PriceRepository extends JpaRepository<Price, Long> {
    @Query(value = "SELECT * FROM prices p " +
            "WHERE p.brand_id = ?1 AND " +
            "p.product_id = ?2 AND " +
            "?3 BETWEEN p.start_date AND p.end_date " +
            "ORDER BY p.priority DESC LIMIT 1", nativeQuery = true)
    Price getPriceForProductAndBrand(Integer brandId, Integer productId, Date date);
}
