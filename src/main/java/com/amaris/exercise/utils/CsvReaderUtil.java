package com.amaris.exercise.utils;

import com.amaris.exercise.model.Price;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class CsvReaderUtil {

    public static String TYPE = "text/csv";

    private static final String HEADER_BRAND_ID = "BrandId";
    private static final String HEADER_STARTDATE = "StartDate";
    private static final String HEADER_ENDDATE = "EndDate";
    private static final String HEADER_PRICELIST = "PriceList";
    private static final String HEADER_PRODUCTID = "ProductId";
    private static final String HEADER_PRIORITY = "Priority";
    private static final String HEADER_PRICE = "Price";
    private static final String HEADER_CURRENCY = "Currency";
    private static final String HEADER_LASTUPDATE = "LastUpdate";
    private static final String HEADER_LASTUPDATEBY = "LastUpdateBy";


    public static List<Price> csvToPrice(String filePath) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
        CSVParser csvParser = new CSVParser(fileReader,
                CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");

        List<CSVRecord> recordList = csvParser.getRecords();

        return recordList.stream().map(r -> {
            try {
                return Price.builder().
                        brandId(Integer.parseInt(r.get(HEADER_BRAND_ID))).
                        startDate(sdf.parse(r.get(HEADER_STARTDATE))).
                        endDate(sdf.parse(r.get(HEADER_ENDDATE))).
                        priceList(Integer.parseInt(r.get(HEADER_PRICELIST))).
                        productId(Integer.parseInt(r.get(HEADER_PRODUCTID))).
                        priority(Integer.parseInt(r.get(HEADER_PRIORITY))).
                        price(Double.parseDouble(r.get(HEADER_PRICE))).
                        curr(r.get(HEADER_CURRENCY)).
                        lastUpdate(sdf.parse(r.get(HEADER_LASTUPDATE))).
                        lastUpdatedBy(r.get(HEADER_LASTUPDATEBY)).build();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }).collect(Collectors.toList());
    }
}
