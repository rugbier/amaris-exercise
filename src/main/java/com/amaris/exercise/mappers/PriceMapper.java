package com.amaris.exercise.mappers;

import com.amaris.exercise.dto.PriceDto;
import com.amaris.exercise.model.Price;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PriceMapper {
    PriceMapper INSTANCE = Mappers.getMapper(PriceMapper.class);

    PriceDto toPriceDto(Price price);
}
